package pl.edu.pwr;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

class LibraryLocal {
    public String loadFileFromPath(String filePath){
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
        {

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public void transformContent(String filePath, String content, boolean chMistake, boolean uMistake,
                                 boolean czechMistake, boolean spaceEater) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        if (chMistake){
            content = makeChMistake(content);
        }
        if (uMistake){
            content = makeUMistake(content);
        }
        if (czechMistake){
            content = makeCzechMistake(content);
        }
        if (spaceEater){
            content = makeSpaceEater(content);
        }
        fileWriter.write(content);
        fileWriter.close();
    }

    public String makeChMistake(String content){
        char[] contentArray = content.toCharArray();
        ArrayList<Integer> zIndexes = new ArrayList();

        for (int i=0 ; i<contentArray.length - 1; i++){
            if (contentArray[i] == 'c' & contentArray[i+1] =='h'){
                Random rand = new Random();
                int a = rand.nextInt(100);
                if(a > 60) {
                    contentArray[i] = 'h';
                    zIndexes.add(i+1);
                }
            }
        }
        content = String.valueOf(contentArray);
        StringBuilder builder = new StringBuilder(content);
        int counter = 0;
        for(int i = 0; i< zIndexes.size(); i++) {
            builder.deleteCharAt(zIndexes.get(i) - counter);
            counter++;
        }
        content = builder.toString();
        return content;
    }
    public String makeUMistake(String content){
        char[] contentArray = content.toCharArray();

        for (int i=0; i<contentArray.length; i++) {
            if (contentArray[i] == 'u') {
                Random rand = new Random();
                int a = rand.nextInt(100);
                if(a > 60) {
                    contentArray[i] = 'ó';
                }
            }
        }
        content = String.valueOf(contentArray);
        StringBuilder builder = new StringBuilder(content);
        content = builder.toString();


        return content;

    }
    public String makeCzechMistake(String content){
        char[] contentArray = content.toCharArray();

        for(int i=0; i<contentArray.length/10; i++){
            Random rand = new Random();
            int a = rand.nextInt(contentArray.length-1);
            if(contentArray[a] != ' '|| contentArray[a+1] != ' '){
                char temp = contentArray[a];
                contentArray[a] = contentArray[a+1];
                contentArray[a+1] = temp;
            }
        }
        content = String.valueOf(contentArray);
        System.out.println(content);
        return content;
    }

    public String makeSpaceEater(String content){
        StringBuilder builder = new StringBuilder(content);
        for ( int i=0; i<builder.length(); i++){
            if (builder.charAt(i) == ' ') {
                Random rand = new Random();
                int a = rand.nextInt(100);
                if (a > 60) {
                    builder.deleteCharAt(i);
                    i--;
                }
            }
        }
        content = builder.toString();
        System.out.println(content);
        return content;

    }
}
